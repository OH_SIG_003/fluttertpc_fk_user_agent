import 'package:fk_user_agent_ohos/fk_user_agent_ohos.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const MethodChannel channel = MethodChannel('fk_user_agent');
  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(FkUserAgentOhos.userAgent, isNotEmpty);
    });
  });
  test('webViewUserAgent', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(FkUserAgentOhos.webViewUserAgent, isNotEmpty);
    });
  });
  test('getProperty', () async {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      expect(FkUserAgentOhos.getProperty('webViewUserAgent'), isNotEmpty);
    });
  });
}
